import React, { Component } from 'react';
import { 
    Title, Header, Container, 
    Left, Button, Icon, Body, Tab, Tabs,
    TabHeading, Text, Drawer, Right, Footer
} 
from 'native-base';
import IncomeList from './servant/IncomeList';
import SpendingList from './servant/SpendingList';
import { DrawerPage } from './Drawer/DrawerPage';
import ResultData from '../components/ResultData';

export default class Home extends Component {

    closeDrawer() {
        this.drawer._root.close();
      }

      openDrawer() {
        this.drawer._root.open();
      }

    render() {
        return (
            <Drawer
            ref={(ref) => { this.drawer = ref; }}
            content={<DrawerPage navigator={this.navigator} />}
            onClose={() => this.closeDrawer()}
            >
                <Container>
                    <Header hasTabs>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon name='menu' />
                            </Button>
                        </Left>

                        <Body>
                            <Title>Home</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Tabs>
                        <Tab heading={<TabHeading><Text> Pemasukan </Text></TabHeading>} >
                            <IncomeList />
                        </Tab>
                        <Tab heading={<TabHeading><Text> Pengeluaran </Text></TabHeading>} >
                            <SpendingList />
                        </Tab>
                    </Tabs>

                    <Footer>
                        <ResultData />
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}
