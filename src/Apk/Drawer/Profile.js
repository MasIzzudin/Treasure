import React, { Component } from 'react';
import { ImageBackground } from 'react-native';
import firebase from 'firebase';
import { Form, Thumbnail, Text } from 'native-base';
import iconProfile from '../../img/account.png';
import backgroundImage from '../../img/drawerpic.jpg';

export class Profile extends Component {
    render() {
        const { currentUser } = firebase.auth();
        return (
            <ImageBackground source={backgroundImage} style={{ flex: 1 }} >
                <Form style={{ flex: 1, backgroundColor: 'rgba(158, 192, 247, 0.5)' }}>
                    <Form style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Form style={Style.wrapThumbnail}>
                            <Thumbnail source={iconProfile} style={{ width: 80, height: 80 }} />
                        </Form>
                    </Form>

                    <Form>
                        <Text style={Style.wrapText}>
                        Hello {currentUser.email}
                        </Text>
                    </Form>
                </Form>
            </ImageBackground>
        );
    }
}

const Style = {
    wrapThumbnail: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 80,
        height: 80
    },

    wrapText: {
        fontSize: 15,
        color: 'black',
        marginBottom: 10,
        marginLeft: 5
    }
};
