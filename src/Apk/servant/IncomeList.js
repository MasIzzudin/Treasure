import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { IncomeData } from '../../actions';
import ListTasks from './ListTasks';

class IncomeList extends Component {
    componentWillMount() {
        this.props.IncomeData();
        
        this.createDataSource(this.props);
    }

    componentWillReceiveProps(NextProps) {
        this.createDataSource(NextProps);
    }

    createDataSource({ valueArray }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(valueArray);
    }

    renderRow(income) {
        return <ListTasks income={income} />;
    }

    render() {
        return (
            <ListView 
                enableEmptySections
                dataSource={this.dataSource}
                renderRow={this.renderRow}
            />
        );
    }
}

const mapStateToProps = state => {
    const valueArray = _.map(state.incomeVal, (val, uid) => {
        return { ...val, uid };
    });
    return { valueArray };
};

export default connect(mapStateToProps, { IncomeData })(IncomeList);
