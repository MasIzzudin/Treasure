import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { SpendingData } from '../../actions';
import ListTaskSpending from './ListTaskSpending';

class IncomeList extends Component {
    componentWillMount() {
        this.props.SpendingData();
        this.createDataSource(this.props);
    }

    componentWillReceiveProps(NextProps) {
        this.createDataSource(NextProps);
    }

    createDataSource({ valueArray }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(valueArray);
    }

    renderRow(income) {
        return <ListTaskSpending income={income} />;
    }

    render() {
        return (
            <ListView 
                enableEmptySections
                dataSource={this.dataSource}
                renderRow={this.renderRow}
            />
        );
    }
}

const mapStateToProps = state => {
    const valueArray = _.map(state.spendingVal, (val, uid) => {
        return { ...val, uid };
    });
    return { valueArray };
};

export default connect(mapStateToProps, { SpendingData })(IncomeList);
